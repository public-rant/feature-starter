# Panoptical

> I've had this dream since before Rails was released to build a web interface for developing Rails itself. We've long had little drops of this idea,... So rather than wait for some grand vision to come together, I'm just getting started with what we got and going from there.
>> @dhh, [The Rails Conductor](https://github.com/rails/rails/pull/35489)

---

## Does it solve a real problem for GitLab users? 

*Does the project clearly articulate the problem it aims to address for GitLab users? Effective solutions are not solely technical but can also drive social impact. Explain why this problem is significant and how your solution effectively resolves it.*

> People who use A.I will replace people who don't
>> Andrew Ng, [Wall Street Journal News](https://www.youtube.com/watch?v=-mIjwN1o7nE)

Panoptical bridges the gap between thought and action. With Panoptical, you can build your product using nothing other than your existing product management tools. Configure APIs with custom fields, code features with Natural Language. Test features in isolated sandboxes before releasing them to production.

Panoptical uses webhooks to listen for events which prompt
Make comments on merge requests or tickets in your project management software. Observe the changes deployed to codesandbox. Deploy devcontainer build artifacts to production. Track changes with your calendar.

## Innovativeness

*Does the project highlight the unique features of your solution and how it differs from existing solutions? Show the creativity and originality in your approach.*

Mojo is a new programming language which is faster than C and is compabtible with the Python ecosystem of machine learning libraries.
Using the existing `calcurse-caldav` utility, we can syncronise with any calendar or project management tool. Using the scaffolding of a devcontainer together with webhooks which listen for events which should prompt the system, we can implement something like an opensource version of getclockwise, or the rails conductor. And everything is securely backed up with restic.

## Overall Quality

*Did you demonstrate excellence in the execution of your idea, including design, user experience, and technical implementation?*

With a little bit more time, this could be made into a production ready system.

## Scalability
*Can your solution grow in terms of customer base, revenue, and operations without a significant drop in performance or quality?*

Mojo adds a lot of bandwidth. The system is able to scale in performance alongside the performce gains of proprietary and open source models.

## Total Addressable Market (TAM)
*What is the potential impact opportunity if your solution was to capture the entire market segment it&#39;s targeting?*

Initially developers wanting to deploy branches to sandboxed environments, then eventually anyone who uses Project Management Software or a Calendar.

## Feasibility
*How practical is your solution in terms of available resources, technology, and time? Consider any potential barriers to success.*

See [`NOTES.md`](src/panoptical/NOTES.md)


